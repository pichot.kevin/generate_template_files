# Generate.py

## Purpose

`generate.py` was develop in order to avoid redundant copy/paste code.

It separates generated files with a MVC pattern by default (configurable).

## How to use

```shell
$> python -m venv venv
```

**Windows**

```shell
$> venv\Script\activate.bat
```

**Unix**

```shell
$> source venv/bin/activate
```

**All**

```shell
(venv) $> pip install -r requirements.txt
(venv) $> python generate.py [name]
```

List of potential improvments:

- interactive configuration (project directory / project language / etc.)
- multiple config.ini files based on projects (config package)
- other templates (only Python files based on MVC pattern + DRF serializers for now. Could be HTML / JavaScript templates for non-Node users for exemple)
- file generation like Angular-cli (`generate.py app_name [file_type] file_name` with `file_type` as model, view, controler, serializer)
- integration with Django (line insertion into `urls.py` with routers / pattern_url / etc.
