import configparser
import json
import os
import sys

from inflection import camelize, tableize, underscore
from jinja2 import Environment, FileSystemLoader, select_autoescape


def check_existing_file(file):
    os.stat(file)
    answer = input(f"File [{file}] already exists, replace? [N/y]:").lower()
    if len(answer):
        if answer in ['y', 'yes']:
            return True
        return False
    return False


def file_to_create(full_path):
    to_create = full_path.copy()
    for key, file in zip(to_create.keys(), to_create.values()):
        try:
            if not check_existing_file(file):
                del(full_path[key])
        except FileNotFoundError:
            pass


def check_files():
    dir_paths = {
        f'{key}': f"{config['project'][key]}"
            for key in keywords
    }
    full_paths = {
        f'{key}': f"{val}{underscore(class_name)}{config['extensions'][key]}"
            for key, val in zip(dir_paths.keys(),
                                dir_paths.values())
    }
    file_to_create(full_paths)
    if len(full_paths):
        print("Creating following files:")
        for key, file in zip(full_paths.keys(), full_paths.values()):
            print(file, end='')
            template = env.get_template(config['templates'][key])
            with open(file, 'w+') as f:
                print(template.render(class_name=camelize(class_name),
                                      var_name=underscore(class_name),
                                      tab_name=tableize(class_name)),
                      file=f)
            print("...Done")
    else:
        print("No file to create")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        raise Exception("Only need a name to generate")

    class_name = sys.argv[1]

    config = configparser.ConfigParser(
        interpolation=configparser.ExtendedInterpolation())
    config.read('config.ini')
    keywords = config['project']['keywords'].split(';')

    try:
        template_dir = config['templates']['directory']
    except KeyError:
        template_dir = 'templates'
    templateLoader = FileSystemLoader(template_dir)
    env = Environment(
        loader=templateLoader,
        autoescape=select_autoescape(['html', 'xml'])
    )
    check_files()
