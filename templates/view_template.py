from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from ..db_managers import {{class_name}}Manager
from ..serializers import (
    {{class_name}}DetailsSerializer,
    {{class_name}}CreationSerializer
)


class {{class_name}}ViewSet(viewsets.ViewSet):
    serializer_class = {{class_name}}DetailsSerializer
    _m = {{class_name}}Manager()

    def list(self, request):
        data = self._m.get_all()
        serializer = {{class_name}}DetailsSerializer(
            instance=data, many=True, context={'request': request}
        )
        return Response(serializer.data)

    def create(self, request):
        serializer = {{class_name}}CreationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        new_obj = self._m.create(serializer.validated_data)
        if new_obj is None:
            return Response(serializer.errors, status=status.HTTP_409_CONFLICT)
        serializer = {{class_name}}DetailsSerializer(
            new_obj, context={'request': request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        obj = self._m.get_by('id_obj', pk)
        if obj is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = {{class_name}}DetailsSerializer(
            obj, context={'request': request}
        )
        return Response(serializer.data)

    def update(self, request, pk=None):
        serializer = {{class_name}}CreationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        obj = self._m.update(pk, serializer.data)
        if obj is None:
            return Response(serializer.errors, status=status.HTTP_409_CONFLICT)
        serializer = {{class_name}}DetailsSerializer(
            obj, context={'request': request}
        )
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        if pk is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        self._m.delete(pk)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_permissions(self):
        return []
