class {{class_name}}:
    table = '{{tab_name}}'
    columns = ['id_{{var_name}}']
    col_types = ['bigserial']
    col_id = columns[0]

    select_query = f"select * from \"{table}\""
    insert_query = f"insert into \"{table}\" (" + ','.join(columns[1:]) + ") values "

    def __init__(self, id_obj=None):
        self.id_{{var_name}} = id_obj
        self.pk = self.id_{{var_name}}

    def get_all(self, custom_filter=None):
        if custom_filter:
            return self.select_query + f" {custom_filter}"
        return self.select_query

    def get_by(self, attr):
        pass

    def insert(self):
        pass

    def get_last_id_created(self):
        query = f"SELECT {self.col_id} FROM {self.table} ORDER BY {self.col_id} DESC LIMIT 1"
        return query

    def get_others(self):
        query = self.select_query + f" WHERE {self.col_id} != {self.id_{{var_name}}}"
        return query

    def update(self):
        attrs = self.__dict__
        query = f"UPDATE \"{self.table}\" SET "
        query += ','.join([f"\"{key}\" = '{val}'" for key, val in zip(self.columns, attrs.values())])
        query += f" WHERE {self.col_id} = {self.id_{{var_name}}}"
        return query

    def delete(self):
        query = f'DELETE FROM {self.table} WHERE {self.col_id} = {self.id_{{var_name}}};'
        return query

    def create_table(self):
        query = f'CREATE TABLE "{self.table}" '
        query += "(" + ','.join(f'"{col_name}" {col_type}'
                                for col_name, col_type in zip(self.columns, self.col_types)) + ")"
        return query
