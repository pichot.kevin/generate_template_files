from rest_framework import serializers


class {{class_name}}DetailsSerializer(serializers.Serializer):
    # id = serializers.ReadOnlyField()
    # name = serializers.CharField(allow_null=True, allow_blank=True)
    # type = serializers.ChoiceField(['line', 'point'])
    # size = serializers.DecimalField(3, 2, default=1.00)
    # url = serializers.HyperlinkedIdentityField(view_name='object-detail', read_only=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def save(self):
        pass


class {{class_name}}CreationSerializer({{class_name}}DetailsSerializer):
    url = None
