from ..models import {{class_name}}


class {{class_name}}Manager:

    def __init__(self):
        pass

    def get_all(self, custom_filter=None):
        pass

    def get_by(self, attr, value):
        pass

    def create(self, data):
        pass

    def update(self, pk, new_data):
        pass

    def delete(self, pk):
        pass
